import sys
from input_reader import InputReader

class GearRatios(InputReader):
    not_symbols = "0123456789."
    digits_str = "0123456789"

    def __init__(self, file_name: str) -> None:
        super().__init__(file_name)
        self.parts = []
        self.init_parts()

    def init_parts(self):
        for line_index in range(len(self.lines)):
            line = self.lines[line_index]
            col_index = 0
            while col_index < len(line):
                # skip symbols and .
                if not line[col_index] in GearRatios.digits_str:
                    col_index += 1
                    continue
                
                # find number bounds
                col_index_end = col_index+1
                while col_index_end < len(line) and line[col_index_end] in GearRatios.digits_str:
                    col_index_end += 1

                # test if number is a part number
                has_symbol,symbol_line_index,symbol_col_index = self.has_adjascent_symbol(line_index, col_index, col_index_end)
                if (has_symbol):
                    part_number = int(line[col_index:col_index_end])
                    self.parts.append( {
                        "number":part_number,
                        "symbol": self.lines[symbol_line_index][symbol_col_index],
                        "symbol_line": symbol_line_index,
                        "symbol_col": symbol_col_index
                    })
                
                col_index = col_index_end

    def has_adjascent_symbol(self, line_index, col_index_begin, col_index_end):
        # check left
        if (col_index_begin > 0):
            col_index_begin -= 1
            if not self.lines[line_index][col_index_begin] in GearRatios.not_symbols:
                return (True, line_index, col_index_begin)
        # check right
        if (col_index_end < len(self.lines[line_index])):
            if not self.lines[line_index][col_index_end] in GearRatios.not_symbols:
                return (True, line_index, col_index_end)
            col_index_end += 1
        # check above
        if line_index > 0:
            for col_index in range(col_index_begin, col_index_end):
                if not self.lines[line_index-1][col_index] in GearRatios.not_symbols:
                    return (True, line_index-1, col_index)
        # check below
        if line_index < len(self.lines)-1:
            for col_index in range(col_index_begin, col_index_end):
                if not self.lines[line_index+1][col_index] in GearRatios.not_symbols:
                    return (True, line_index+1, col_index)
                
        return (False, -1, -1)
    
    def get_sum_of_part_numbers(self):
        sum = 0
        for part in self.parts:
            sum += part["number"]
        return sum

    def get_sum_of_gear_ratios(self):
        # get all parts with symbol '*'
        gears_candidates = []
        for part in self.parts:
            if part["symbol"] == '*':
                gears_candidates.append(part)
        
        sum = 0
        for i in range(len(gears_candidates)-1):
            is_gear = False
            candidate = gears_candidates[i]
            symbol_line = candidate["symbol_line"]
            symbol_col = candidate["symbol_col"]
            gear_ratio = candidate["number"]
            for j in range(i+1, len(gears_candidates)):
                candidate2 = gears_candidates[j]
                if candidate2["symbol_line"] == symbol_line and candidate2["symbol_col"] == symbol_col:
                    # if it's the first we find, might be ok
                    if not is_gear:
                        is_gear = True
                        gear_ratio *= candidate2["number"]
                    else:
                        # it's the third number around this part, so it's not a gear
                        is_gear = False
                        break
            if is_gear:
                sum += gear_ratio
        return sum

    
if __name__ == "__main__":
    file_name = sys.argv[1]
    g = GearRatios(file_name)
    part_sum = g.get_sum_of_part_numbers()
    print("Part numbers sum is :", part_sum)
    gear_ratio_sum = g.get_sum_of_gear_ratios()
    print("Gear ratios sum is :", gear_ratio_sum)