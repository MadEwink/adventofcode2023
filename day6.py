import sys
import math
import parse
from input_reader import InputReader

class BoatRecords(InputReader):
    def __init__(self, file_name: str) -> None:
        super().__init__(file_name)
        self.times = self.parseRecords(0)
        self.distances = self.parseRecords(1)
        self.unique_time = self.parseUnique(0)
        self.unique_dist = self.parseUnique(1)
        assert(len(self.times) == len(self.distances))
    
    def parseRecords(self, line_index : int):
        _,record_str = parse.parse("{}: {}", self.lines[line_index])
        record_array = []
        for r in record_str.split(' '):
            if (len(r) > 0):
                record_array.append(int(r))
        return record_array

    def parseUnique(self, line_index : int):
        _,record_str = parse.parse("{}: {}", self.lines[line_index])
        record_array = []
        for r in record_str.split(' '):
            if (len(r) > 0):
                record_array.append(r)
        return int(''.join(record_array))
        
    def get_discr(time : int, distance : int) -> float:
        return time*time - 4*distance

    def clamp_time(time : int, max_time : int) -> int:
        return max(min(time, max_time), 0)
    
    # Since we want a distance strictly greater than the record we want to substract one if it's already an integer
    def upper(t : float) -> int:
        return int(t)+1

    # Same logic : if it was an integer, substract one, else we want the integer below, so ceil-1 would be the same as floor
    def lower(t : float) -> int:
        return math.ceil(t)-1
    
    def get_winning_boundaries_time(time, distance) -> (int,int):
        discr = BoatRecords.get_discr(time, distance)
        if (discr <= 0):
            return None
        discr_sqr = math.sqrt(discr)
        min_time = BoatRecords.upper((time - discr_sqr)/2)
        max_time = BoatRecords.lower((time + discr_sqr)/2)
        return (BoatRecords.clamp_time(min_time, time), BoatRecords.clamp_time(max_time, time))
    
    def get_product_of_winning_ways(self):
        prod = 1
        for i in range(len(self.times)):
            min_time, max_time = BoatRecords.get_winning_boundaries_time(self.times[i], self.distances[i])
            prod *= max_time-min_time+1
        return prod
    
    def get_number_of_winning_unique(self):
        min_time, max_time = BoatRecords.get_winning_boundaries_time(self.unique_time, self.unique_dist)
        return max_time-min_time+1

    
if __name__ == "__main__":
    file_name = sys.argv[1]
    b = BoatRecords(file_name)
    product = b.get_product_of_winning_ways()
    print("Product is :", product)
    ways = b.get_number_of_winning_unique()
    print("Number of ways to win longer :", ways)