import sys
import parse
from input_reader import InputReader

class Range:
    def __init__(self, category : str, range_start : int, range_length : int) -> None:
        self.category = category
        self.range_start = range_start
        self.range_length = range_length

class AlmanacMap:
    def __init__(self, source : str, dest : str, dest_range_start : int, source_range_start : int, range_length : int) -> None:
        self.source = source
        self.dest = dest
        self.dest_range_start = dest_range_start
        self.source_range_start = source_range_start
        self.range_length = range_length
    
    def is_in_source_range(self, number : int) -> int:
        return self.source_range_start <= number and number < self.source_range_start + self.range_length
    
    def get_dest_number(self, source_number : int) -> int:
        dist = source_number - self.source_range_start
        return self.dest_range_start + dist
    
    def get_dest_ranges(self, source_range : Range) -> list[Range]:
        assert(source_range.category == self.source)

        start = source_range.range_start
        end = source_range.range_start+source_range.range_length

        # if not intersecting, return as is
        if (start >= self.source_range_start+self.range_length or end <= self.source_range_start):
            return [source_range]
        dest_ranges : list[Range] = []
        # trim begining
        if (start < self.source_range_start):
            dest_ranges.append(Range(self.source, start, self.source_range_start - start))
            start = self.source_range_start
        # trim end
        if (end > self.source_range_start+self.range_length):
            dest_ranges.append(Range(self.source, self.source_range_start+self.range_length, end - (self.source_range_start+self.range_length)))
            end = self.source_range_start+self.range_length
        # add included part -> we just have to offset range start
        dest_start = self.dest_range_start + (start - self.source_range_start)
        dest_ranges.append(Range(self.dest, dest_start, end-start))

        return dest_ranges

class Almanac(InputReader):
    def __init__(self, file_name: str) -> None:
        super().__init__(file_name)
        self.parse_seed_line()
        self.parse_maps()

    def parse_seed_line(self):
        seed_line = self.lines[0]
        seeds_str = parse.parse('seeds: {}', seed_line)[0]
        self.seeds = [ int(seed_str) for seed_str in seeds_str.split(' ')]
    
    def parse_maps(self):
        self.maps_dict = {}
        cur_map_source = None
        cur_map_dest = None
        for line in self.lines[1:]:
            if len(line) == 0:
                continue
            map_parse = parse.parse("{}-to-{} map:", line)
            if map_parse != None:
                cur_map_source,cur_map_dest = map_parse
                self.maps_dict[cur_map_source] = []
                continue
            number_str_list = line.split(" ")
            dest_range_start = int(number_str_list[0])
            source_range_start = int(number_str_list[1])
            range_length = int(number_str_list[2])
            self.maps_dict[cur_map_source].append(AlmanacMap(cur_map_source, cur_map_dest, dest_range_start, source_range_start, range_length))
    
    def get_lowest_location_number(self):
        current_step = "seed"
        values_array = self.seeds.copy()
        while (current_step != "location"):
            current_maps = self.maps_dict[current_step]
            for i in range(len(values_array)):
                number = values_array[i]
                for map in current_maps:
                    if map.is_in_source_range(number):
                        values_array[i] = map.get_dest_number(number)
                        break
            current_step = current_maps[0].dest
        return min(values_array)
    
    def get_lowest_location_number_from_range(self) -> int:
        ranges : list[Range] = []
        for index in range(int(len(self.seeds)/2)):
            i = 2*index
            ranges.append(Range("seed", self.seeds[i], self.seeds[i+1]))
        current_step = "seed"
        while (current_step != "location"):
            current_maps = self.maps_dict[current_step]
            dest_ranges : list[Range] = []
            for map in current_maps:
                unchanged_ranges : list[Range] = []
                # process all ranges with current map
                for cur_range in ranges:
                    new_ranges : list[Range] = map.get_dest_ranges(cur_range)
                    for new_range in new_ranges:
                        if new_range.category == current_step:
                            unchanged_ranges.append(new_range)
                        else:
                            dest_ranges.append(new_range)
                # update ranges for the next map
                ranges = unchanged_ranges
            # all remaining ranges at this point have same range as destination
            current_step = current_maps[0].dest
            for cur_range in ranges:
                dest_ranges.append(Range(current_step, cur_range.range_start, cur_range.range_length))
            ranges = dest_ranges
        # return min of ranges
        return min([range.range_start for range in ranges])

if __name__ == "__main__":
    file_name = sys.argv[1]
    a = Almanac(file_name)
    lowest_location = a.get_lowest_location_number()
    print("Lower location number is :", lowest_location)
    lowest_location2 = a.get_lowest_location_number_from_range()
    print("Lower location number from range is :", lowest_location2)
