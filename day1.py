import sys
from collections.abc import Callable
from input_reader import InputReader

class Trebuchet(InputReader):
    digits_str = "0123456789"
    digits_spells = ["zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]

    def get_digit_at_index(line : str, index : int) -> int:
        if (line[index] in Trebuchet.digits_str):
            return int(line[index])
        return -1
        
    def get_digit_spell_at_index(line : str, index : int) -> int:
        if (line[index] in Trebuchet.digits_str):
            return int(line[index])

        for digit_index in range(len(Trebuchet.digits_spells)):
            digit_spell = Trebuchet.digits_spells[digit_index]
            if (len(line) < len(digit_spell)+index):
                continue
            match = True
            for i in range(len(digit_spell)):
                if (digit_spell[i] != line[index+i]):
                    match = False
                    break
            if not match:
                continue

            return digit_index

        return -1

    def get_calibration_value(line : str, get_digit_func : Callable[[str,int], int]) -> int:
        # get first and last digits
        first_digit = 0
        last_digit = 0
        for i in range(len(line)):
            v = get_digit_func(line, i)
            if v >= 0:
                first_digit = v
                break;
        for i in range(len(line)):
            j = len(line) - i - 1
            v = get_digit_func(line, j)
            if v >= 0:
                last_digit = v
                break;
        
        return first_digit*10 + last_digit
    
    def get_calibration_sum(self, get_digit_func : Callable[[str, int], int]) -> int:
        sum = 0
        for line in self.lines:
            sum += Trebuchet.get_calibration_value(line, get_digit_func)
        return sum

if __name__ == "__main__":
    file_name = sys.argv[1]
    t = Trebuchet(file_name)
    calibration_sum = t.get_calibration_sum(Trebuchet.get_digit_at_index)
    print("Sum is :", calibration_sum)
    calibration_sum = t.get_calibration_sum(Trebuchet.get_digit_spell_at_index)
    print("Corrected sum is :", calibration_sum)