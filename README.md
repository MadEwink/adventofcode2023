# Advent of Code 2023

This year I'm doing Advent of Code in python with no ambition whatsoever.  
I'll try my best to keep up, but I'll most probably do challenges on week-ends.

## Usage

For each day just type `python <dayX.py> <path-to-input>`.

## Dependencies

- Day 2 depends on `parse`