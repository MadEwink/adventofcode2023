import sys
import parse
from input_reader import InputReader

class CubeConundrum(InputReader):
    target_composition = {"red": 12, "green": 13, "blue": 14 }

    def __init__(self, file_name: str) -> None:
        super().__init__(file_name)
        self.games = []
        for line in self.lines:
            index,composition = parse.parse("Game {:d}: {}", line)
            self.games.append(CubeConundrum.parse_composition(composition))

    def parse_composition(composition_str : str):
        compositions = []
        game_subsets = composition_str.split(';')
        for game_subset in game_subsets:
            composition = {}
            composition_split = game_subset.split(',')
            for composition_component in composition_split:
                number,color = parse.parse("{:d} {}", composition_component)
                if (color in composition):
                    composition[color] += number
                else:
                    composition[color] = number
            compositions.append(composition)
        return compositions
    
    def is_game_possible(self, game_index, target_composition):
        game = self.games[game_index]
        for subset in game:
            for color in subset:
                if not color in target_composition:
                    return False
                if subset[color] > target_composition[color]:
                    return False
        return True
    
    def get_sum_of_possible_ids(self, target_composition):
        sum = 0
        for index in range(len(self.games)):
            id = index+1
            if self.is_game_possible(index, target_composition):
                sum += id
        return sum
    
    def get_minimal_composition(self, game_index):
        minimal_composition = {}
        game = self.games[game_index]
        for subset in game:
            for color in subset:
                if color in minimal_composition:
                    minimal_composition[color] = max(minimal_composition[color], subset[color])
                else:
                    minimal_composition[color] = subset[color]
        return minimal_composition
    
    def get_composition_power(composition):
        power = 1
        for color in composition:
            power *= composition[color]
        return power
    
    def get_minimal_power_sum(self):
        sum = 0
        for i in range(len(self.games)):
            minimal_composition = self.get_minimal_composition(i)
            power = CubeConundrum.get_composition_power(minimal_composition)
            sum += power
        return sum

if __name__ == "__main__":
    file_name = sys.argv[1]
    c = CubeConundrum(file_name)
    id_sum = c.get_sum_of_possible_ids(CubeConundrum.target_composition)
    print("Id sum is :", id_sum)
    power_sum = c.get_minimal_power_sum()
    print("Power sum is :", power_sum)