import sys
import parse
from input_reader import InputReader

class Card:
    def __init__(self, line) -> None:
        self.id = 0
        self.winning_list = []
        self.number_list = []
        self.parse_line(line)
        self.matching_number = self.get_matching_number()

    def parse_line(self, line):
        _,id,winning_line,number_line = parse.parse('Card{:^}{:d}: {} | {}', line)
        self.id = id
        for winning_str in winning_line.split(' '):
            if len(winning_str) == 0:
                continue
            self.winning_list.append(int(winning_str))
        for number_str in number_line.split(' '):
            if len(number_str) == 0:
                continue
            self.number_list.append(int(number_str))
    
    def get_matching_number(self) -> int:
        n = 0
        for number in self.number_list:
            if number in self.winning_list:
                n += 1
        return n
    
    def get_points(self) -> int:
        if self.matching_number == 0:
            return 0
        return pow(2, self.matching_number-1)

class Scratchcards(InputReader):
    def __init__(self, file_name: str) -> None:
        super().__init__(file_name)

        self.cards = []
        for line in self.lines:
            self.cards.append(Card(line))
    
    def get_sum_of_points(self) -> int:
        sum = 0
        for card in self.cards:
            sum += card.get_points()
        return sum
    
    def get_sum_of_won_cards(self) -> int:
        sum = 0
        card_number_len = len(self.cards)
        card_number_list = [1]*card_number_len
        for i in range(card_number_len):
            sum += card_number_list[i]
            number = self.cards[i].matching_number
            for j in range(1, number+1):
                if (i+j >= card_number_len):
                    break
                card_number_list[i+j] += card_number_list[i]
        return sum

if __name__ == "__main__":
    file_name = sys.argv[1]
    s = Scratchcards(file_name)
    points_sum = s.get_sum_of_points()
    print("Points sum is :", points_sum)
    card_number_sum = s.get_sum_of_won_cards()
    print("Number of card won is :", card_number_sum)